CC = clang
SD = ./src
BD = ./build

GLD = ./lib/glad
GLD_SRC = $(GLD)/src
GLD_INC = $(GLD)/include

LIBS = -lm -lglfw  -lX11 -lpthread	-lXrandr -ldl
CFLAGS=  -Wall

.PHONY: all clean
all: clean nubo

nubo: $(GLD_SRC)/glad.c $(SD)/*.c
	$(CC) $(CFLAGS) $(LIBS) -I$(GLD_INC) -o $(BD)/$@ $^

clean:
	rm -f $(BD)/*
