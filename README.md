<h4 align="center">
    <img src="assets/mascot.svg" align="center" width="100"/>
</h4>

<h4 align="center">
  <img src="https://img.shields.io/github/languages/top/LinArcX/nubo.svg"/>  <img src="https://img.shields.io/github/repo-size/LinArcX/nubo.svg"/>  <img src="https://img.shields.io/github/tag/LinArcX/nubo.svg?colorB=green"/>
</h4>

# Nubo
> “The sky, a perfect empty canvas, offers clouds nonetheless. They shift and drift and beg interpretation… such is the nature of art.”― Jeb Dickerson

# Prerequisites
- `make`
- `glfw`
- `glad`

## What does nubo mean?
It's an Esperanto word means: "cloud".

## License
![License](https://img.shields.io/github/license/LinArcX/nubo.svg)
