#include <stdio.h>
#include "base.h"
#include <math.h>

void
process_key_escape(void)
{
    glfwSetWindowShouldClose(fBase.window, 1);
}

void
process_key_q(void)
{
    glfwSetWindowShouldClose(fBase.window, 1);
}

void
custom_background()
{
    const GLfloat color[] = { (float)sin(fBase.current_time) * 0.5f + 0.5f,
                              (float)cos(fBase.current_time) * 0.5f + 0.5f,
                              0.0f,
                              1.0f };
    glClearBufferfv(GL_COLOR, 0, color);
}

void
render()
{
    printf("current time: %f\n", fBase.current_time);
    set_background_color(GRAY);
    process_key(GLFW_KEY_ESCAPE, GLFW_PRESS, process_key_escape);
    process_key(GLFW_KEY_Q, GLFW_PRESS, process_key_q);
}

void
handle_windows_size_changed(void)
{
    printf("window size changed!\n");
    glViewport(0, 0, fBase.width, fBase.height);
}

int
main(int argc, char *argv[])
{
    nubo_init("Hello opengl", 800, 600, render, handle_windows_size_changed);
    return 0;
}
