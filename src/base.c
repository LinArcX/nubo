#include "base.h"
#include <errno.h>
#include <stdio.h>

const struct color RED = { .red = 0.2f,
                           .green = 0.3f,
                           .blue = 0.3f,
                           .alpha = 1.0f };

const struct color LIGHT_BLUE = { .red = 0.6f,
                                  .green = 0.6f,
                                  .blue = 1.0f,
                                  .alpha = 1.0f };

const struct color BLUE = { .red = 0.2f,
                            .green = 0.6f,
                            .blue = 1.0f,
                            .alpha = 1.0f };

const struct color BROWN = { .red = 153 / 255,
                             .green = 76 / 255,
                             .blue = 0 / 255,
                             .alpha = 1.0 };

const struct color LIGHT_GRAY = { .red = 0.75f,
                                  .green = 0.75f,
                                  .blue = 0.75f,
                                  .alpha = 1.0f };

const struct color GRAY = { .red = 0.62f,
                            .green = 0.62f,
                            .blue = 0.62f,
                            .alpha = 1.0f };

const struct color DARK_GRAY = { .red = 0.37f,
                                 .green = 0.37f,
                                 .blue = 0.37f,
                                 .alpha = 1.0f };

NuboBase fBase;

int8_t
nubo_init(char *title,
          uint16_t width,
          uint16_t height,
          void (*render)(void),
          void (*handle_windows_size_changed)(void))
{
    fBase.title = title;
    fBase.width = width;
    fBase.height = height;
    fBase.render = (*render);
    fBase.handle_windows_size_changed = (*handle_windows_size_changed);
    fBase.current_time = glfwGetTime();

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    /* glfw window creation */
    fBase.window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (fBase.window == NULL) {
        fprintf(stderr, "Error creating window.\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(fBase.window);
    glfwSetFramebufferSizeCallback(fBase.window, handle_windows_size_changed);

    /* glad: load all OpenGL function pointers */
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        fprintf(stdout, "Failed to initialize GLAD");
        return -1;
    }

    /* render loop */
    while (!glfwWindowShouldClose(fBase.window)) {
        fBase.current_time = glfwGetTime();

        (*render)();

        glfwSwapBuffers(fBase.window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void
set_background_color(const struct color col)
{
    GLfloat const color[] = { col.red, col.green, col.blue, col.alpha };
    glClearBufferfv(GL_COLOR, 0, color);
}

void
process_key(int key, int event, void (*process_key)())
{
    if (glfwGetKey(fBase.window, key) == event)
        (*process_key)();
}

uint16_t
nubo_base_get_width()
{
    return fBase.width;
}

uint16_t
nubo_base_get_height()
{
    return fBase.height;
}

void
nubo_base_set_width(uint16_t width)
{
    fBase.width = width;
}

void
nubo_base_set_height(uint16_t height)
{
    fBase.height = height;
}
